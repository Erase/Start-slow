[![No Maintenance Intended](http://unmaintained.tech/badge.svg)](http://unmaintained.tech/)

Start slow
--------

**Start slow** est une simple page d'accueil, typiquement adaptée pour la page par défaut de votre navigateur Web.
Elle utilise notamment l'API de [Unsplash](https://unsplash.com/) pour l'affichage des images de fond et un stockage local (*"localSotrage"*) des liens épinglés.

**[Une démo hébergée est disponible par ici : ](https://start.green-effect.fr/) https://start.green-effect.fr/**


Architecture et organisation
------------------------
Le projet est réparti sur deux répertoires : 
 * `dist` : répertoire contenant le projet compilé permettant un usage *"tel quel"*
 * `src` : répertoire contenant les sources du projet

Dans l'éventualité de modifications Javascript ou CSS, vous pouvez relancer la compilation des sources en utilisant `Gulp` et les dépendances définies dans le fichier `package.json`.


Commandes et fonctionnalités
------------------------
Le champ de saisi principal permet d'effectuer une recherche avec le moteur définie par défaut.

Vous pouvez aussi épingler vos propres raccourcis de lien en cliquant sur le bouton associé **(`+`)**.

Pour supprimer un lien précédement ajouté, il suffit de faire un *clic droit* dessus.

Il est possible de réaliser des recherches sur d'autres espaces et moteurs distants à l'aide de raccourcis. Plusieurs sont déjà définis par défaut. Par exemple, pour effectuer une recherche sur Twitter, il suffit de saisir la commande `t:votre_recherche`. La liste des commandes et espaces de recherches associées est consultable en saisissant la commande `?`.


Personnalisation
------------------------
Afin de simplifier la personnalisation du projet, vous pouvez éditer directement le fichier `config.json` et en modifier le contenu par défaut : 
 * `unsplashCollectionID` : identifiant de la collection [Unsplash](https://unsplash.com/) ciblée pour les images de fond
 * `defaultWeatherLocation` : ville ciblée pour déterminer la recherche météo
 * `WeatherKey` : clé API extraite de [OpenWeatherMap](http://openweathermap.org/login) - Non obligatoire mais fortement conseillé, nécessite une simple inscription
 * `defaultSearchBaseURL` : URL du moteur de recherche par défaut
 * `commands` : permet de modifier les moteurs de recherche et leurs commandes de raccourcis
 * `messages` : permet de définir les messages qui s'afficheront selon une plage horaire précise (heure min et heure max)


FAQ
------------------------
> ***Je n'arrive plus à supprimer les liens stockés***

Saisir la commande `!raz` dans le champ texte principal.

> ***Je ne me souviens plus de la liste des moteurs de recherches ou de leurs raccourcis***

Saisir la commande `?` dans le champ texte principal.

> ***Je désire supprimer un lien déjà épinglé***

Clic droit sur le lien concerné.

> ***Quels sont les moteurs de recherche par défaut ?***
 * `s:votre recherche` : effectuer une recherche sur [Shaarlo](https://www.shaarlo.fr)
 * `b:votre recherche` : effectuer une recherche sur l'instance [Googol](https://googol.warriordudimanche.net) de [Bronco](http://warriordudimanche.net/)
 * `g:votre recherche` : effectuer une recherche sur [Github](https://github.com)
 * `m:votre recherche` : effectuer une recherche dans votre compte Gmail (beurk)
 * `t:votre recherche` : effectuer une recherche sur Twitter (beurk)
 * `y:votre recherche` : effectuer une recherche sur Youtube (beurk)

> ***Pourquoi la personnalisation de la ville pour la météo ne se fait pas automatiquement ?***

Parce que par principe, je suis contre les services de détermination de localisation géographique.

> ***Où sont stockées les liens épinglés ?***

Uniquement du côté du visiteur. Le serveur hébergeant **Start-slow** ne stocke rien. Par contre, cela implique que des liens épinglés sur un terminal ne le seront plus sur un autre.

> **J'ai une remarque, un bug à faire remonter ou une envie de faire évoluer le projet**

Super ! Vous pouvez me contacter par email ou faire remonter tout cela par l'intermédiaire de l'espace où sont stockées les sources du projet !


Copain Bronco
--------
Comme toujours, merci à [Bronco](https://warriordudimanche.net/) pour ses outils, une nouvelle fois utilisés :
 * [Googol](https://github.com/broncowdd/googol)
 * ~~[Goofi](https://github.com/broncowdd/goofi)~~


Licence
-------
 Favicon issue d'une collection du site [Flaticon](https://www.flaticon.com)
 En dehors des différentes licences spécifiques aux outils et services utilisés, le reste du code est distribué sous licence [Creative Commons BY-NC-SA 4.0]


Auteur
------
 * [Un simple développeur paysagiste] :) avec un peu de temps libre et aucune prétention - contact_at_green-effect.fr


[//]: # 
   [Creative Commons BY-NC-SA 4.0]: <http://creativecommons.org/licenses/by-nc-sa/4.0/>
   [Un simple développeur paysagiste]: <http://www.green-effect.fr>
