/**
 *  Copyright 2017 Amardeep Rai
 *  
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

'use strict';

var autoprefixer = require('gulp-autoprefixer');
var csso = require('gulp-csso');
var concat = require('gulp-concat');
var del = require('del');
var gulp = require('gulp');
var htmlmin = require('gulp-htmlmin');
var runSequence = require('run-sequence');
var uglify = require('gulp-uglify');

// Set the browser that you want to supoprt
const AUTOPREFIXER_BROWSERS = [
  'ie >= 10',
  'ie_mob >= 10',
  'ff >= 30',
  'chrome >= 34',
  'safari >= 7',
  'opera >= 23',
  'ios >= 7',
  'android >= 4.4',
  'bb >= 10'
];

// Gulp task to minify CSS files
gulp.task('styles', function () {
  return gulp.src(['src/css/jquery-ui.min.css', 'src/css/style.css','src/css/alertify.min.css', 'src/css/bootstrap.min.css'])
    // Auto-prefix css styles for cross browser compatibility
    .pipe(autoprefixer({browsers: AUTOPREFIXER_BROWSERS}))
    // Minify the file
    .pipe(csso())
    .pipe(concat('style.css'))
    // Output
    .pipe(gulp.dest('./dist/css'))
});

// Gulp task to minify JavaScript files
gulp.task('scripts', function() {
  return gulp.src(['src/js/jquery-1.12.0.min.js', 'src/js/jquery-ui.min.js', 'src/js/openweather.min.js', 'src/js/jstorage.min.js', 'src/js/alertify.min.js', 'src/js/app.js'])
    .pipe(concat('script.js'))
    // Output
    .pipe(gulp.dest('./dist/js'))
});

// Gulp task to minify HTML files
gulp.task('pages', function() {
  return gulp.src(['./src/index.html'])
    .pipe(htmlmin({
      collapseWhitespace: true,
      removeComments: true
    }))
    .pipe(gulp.dest('./dist'));
});

// Clean output directory
gulp.task('clean', () => del(['dist']));
gulp.task('copy', function(){
  gulp.src(['./src/fonts/*']).pipe(gulp.dest('./dist/fonts/'));
  gulp.src(['./src/favicon.ico*']).pipe(gulp.dest('./dist/'));
  gulp.src(['./src/img/weather/day/*']).pipe(gulp.dest('./dist/img/weather/day/'));
  gulp.src(['./src/img/weather/night/*']).pipe(gulp.dest('./dist/img/weather/night/'));
  gulp.src(['./src/config.json*']).pipe(gulp.dest('./dist/'));
});

// Gulp task to minify all files
gulp.task('default', ['clean'], function () {
  runSequence(
    'copy',
    'styles',
    'scripts',
    'pages'
  );
});