var unsplashCollectionID = 1053828;
var defaultWeatherLocation = 'Paris';
var WeatherKey = 'c9d49310f8023ee2617a7634de23c2aa';    //~ Go to https://openweathermap.org/api
var defaultSearchBaseURL = 'https://encrypted.google.com/search?q=';
var commands = [
    { "key": "s", "name": "Shaarlo", "url": "https://www.shaarlo.fr", "search": "/index.php?from=20130000&to=90130000&q=" },
    { "key": "b", "name": "Googl by Bronco", "url": "https://googol.warriordudimanche.net", "search": "/?lang=fr&mod=web&q=" },
    { "key": "g", "name": "GitHub", "url": "https://github.com", "search": "/search?q=" },
    { "key": "m", "name": "Gmail", "url": "https://mail.google.com", "search": "/mail/u/0/#search/" },
    { "key": "t", "name": "Twitter", "url": "https://twitter.com", "search": "/search?q=" },
    { "key": "y", "name": "YouTube", "url": "https://www.youtube.com", "search": "/results?search_query=" },
];
var messages = [
    {
        "min": 22, "max": 5, "texte": "Bonne nuit"
    },
    {
        "min": 19, "max": 21, "texte": "Bonne soirée"
    },
    {
        "min": 14, "max": 18, "texte": "Bon après-midi"
    },
    {
        "min": 12, "max": 14, "texte": "Bon appétit"
    },
    {
        "min": 6, "max": 11, "texte": "Bonjour"
    },
];

function loadWeather(location, key) {
    $('#weather').openWeather({
        key: key,
        city: location,
        lang: 'fr',
        units: 'c',
        iconTarget: '.weather-icon',
        customIcons: 'img/weather/',
        success: function(weather) {
            var html = '<div class="weatherinfo">'+weather.temperature.current+'</div>';
            html += ''+weather.city;
            $("#weather").html(html);
        },
        error: function(data) {
            
        }
    });
}

function timedQuotes() {
    var day = new Date();
    var hr = day.getHours();
    var txt = "";

    $.each(messages, function(key, val){
        if(hr >= val.min && hr <= val.max){
            txt = val.texte;
        }
    });

    return txt;
}

function loadRandomBackgroundImage() {
    var imageSrc = 'https://source.unsplash.com/collection/' + unsplashCollectionID + '/1920x1200';
    $('body').css('background-image', 'url(' + imageSrc + ')');
}

/*
    Format
    var links = [
        {
            titre : 'titre',
            cible : 'url',
            key :   'cle'  
        }, 
        {
            titre : 'titre',
            cible : 'url',
            key :   'cle'  
        }
    ];
 */
function loadLinks(){
    var _links = $.jStorage.get('links');
    if(_links && _links.length){
        var html = '';
        $.each(_links, function(k, r){
            html += '<a href="'+r.cible+'" class="small-box" title="'+r.titre+' - Clic droit pour supprimer" data-key="'+r.key+'">'+r.titre+'</a>';
        });
        html += '<span class="small-box" id="add">+</span>';
        $('.grid').html(html);
        $('a.small-box').contextmenu(function(){
            var key = $(this).attr('data-key');
            removeLink(key);
            alertify.success('Le lien a été correctement supprimé.');
            loadLinks();
            return false;
        });
    }else{
        $('.grid').html('<span class="small-box" id="add">+</span>');
    }
    $('#add').click(function(){
    	$titre = $('#titre');
    	$form = $('#addLinkForm');
    	$cible = $('#cible');
    	$titre.val("");
    	$cible.val("");
    	$form.on('submit', function (evt) {
    		evt.preventDefault();
    		var reg = /^(http|https)?:\/\/[a-zA-Z0-9-\.]+\.[a-z]{2,4}/;
    		if($titre && $titre.val() !== "" && $cible && $cible.val() !== "" && reg.test($cible.val())){
    			addLink($titre.val(), $cible.val());
    			alertify.success('Le lien a été correctement ajouté.');
    			alertify.genericDialog().close().set('closable', true);
                $form.unbind('submit');
    		}else{
    			alertify.error('Erreur dans le titre ou la cible du lien.');
    		}	        
	    });
	    alertify.defaults.glossary.title = "Ajouter un lien";
		alertify.genericDialog($form[0]);
		$form.show();

    });
}

function addLink(nom, cible){
    var k = 'k'+$.now();

    var _links = $.jStorage.get('links');
    if(!_links || !_links.length){
        _links = [];
    }
    _links.push({
        titre : nom,
        cible : cible,
        key   : k
    });
    $.jStorage.set('links', _links);
    loadLinks();
}

function removeLink(key){
    var _links = $.jStorage.get('links');
    if(_links && _links.length){
        var index = null;
        $.each(_links, function(k, r){
            if(r.key === key){
                index = k;
            }
        });
        if(index !== null){
            _links.splice(index, 1);
            $.jStorage.set('links', _links);
        }
        
    }
}

$(function() {
    $.getJSON( "config.json", function( data ) {
        $.each( data, function( key, val ) {
            window[key] = val;
        });

        loadRandomBackgroundImage();

        Clock.init();
        Cmdr.init({
            default: defaultSearchBaseURL,
            commands: commands
        });

        $('#quotes').text(timedQuotes());

        loadWeather(defaultWeatherLocation, WeatherKey);

        loadLinks();
    });


    var Clock = {
        el: document.getElementById('js-time'),

        init: function() {
            Clock.setTime();
            setInterval(Clock.setTime, 1000);
        },

        zeros: function(num) {
            return ('0' + num.toString()).slice(-2);
        },

        setTime: function() {
            var date = new Date();
            var hours = Clock.zeros(date.getHours());
            var minutes = Clock.zeros(date.getMinutes());
            var time = hours + ':' + minutes;

            Clock.el.innerHTML = time;
        }
    };

    var Cmdr = {
        searchForm: document.getElementById('js-search'),
        searchText: document.getElementById('js-search-text'),
        searchHelp: document.getElementById('js-help'),

        init: function(opts) {
            Cmdr.default = opts.default;
            Cmdr.commands = opts.commands;
            Cmdr.searchForm.addEventListener('submit', Cmdr.search, false);
        },

        search: function(e) {
            var q = Cmdr.searchText.value;
			
			if (q.search( 'www.' ) == 0){
				window.location.href = 'https://' + q;
				e.preventDefault();
				return false;
			}
			
            if (q === '?') {
                Cmdr.commands.forEach(function(command) {
                    Cmdr.searchHelp.innerHTML += command.key + ': ' + command.name + '\n';
                });

                Cmdr.searchHelp.style.height = Math.ceil(Cmdr.commands.length / 2) + 'rem';
                Cmdr.searchText.value = '';
            }else if( q === '!raz'){
                $.jStorage.set('links', []);
                loadLinks();
                Cmdr.searchText.value = '';
                alertify.success('Les liens épinglés ont été supprimés.');
            } else {
                Cmdr.location = Cmdr.default + encodeURIComponent(q);
                q = q.split(':');

                Cmdr.commands.forEach(function(command) {
                    if (q[0] === command.key) {
                        Cmdr.location = command.url;

                        if (q[1] && command.search) {
                            q.shift();
                            var searchText = encodeURIComponent(q.join(':').trim());
                            Cmdr.location = command.url + command.search + searchText;
                        }
                    }
                });

                window.location.href = Cmdr.location;
            }

            e.preventDefault();
        }
    };

    

    alertify.genericDialog || alertify.dialog('genericDialog',function(){
	    return {
	        main:function(content){
	            this.setContent(content);
	        },
	        setup:function(){
	            return {
	                focus:{
	                    element:function(){
	                        return this.elements.body.querySelector(this.get('selector'));
	                    },
	                    select:true
	                },
	                options:{
	                    
	                    maximizable:false,
	                    resizable:false,
	                    padding:false
	                }
	            };
	        },
	        settings:{
	            selector:undefined
	        }
	    };
	});
}());
